package com.devcamp.s54.resapi;
import java.text.*;
import java.util.Date;
import org.springframework.web.bind.annotation.*;
@RestController
public class Welcome {
    @CrossOrigin
    @GetMapping("/devcamp-welcome")
    public String nice() {
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        Date now= new Date();
        return String.format("Have a nice day. It is %s!.", dateFormat.format(now));
    }
}
