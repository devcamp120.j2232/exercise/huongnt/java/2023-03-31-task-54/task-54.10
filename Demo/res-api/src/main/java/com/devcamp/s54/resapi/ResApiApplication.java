package com.devcamp.s54.resapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ResApiApplication.class, args);
	}

}
